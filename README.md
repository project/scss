CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module allows development with [SCSS](https://sass-lang.com/) using pure
PHP. This is especially useful when developing on platforms that do not
support other SCSS compilation tools like gulp, or prevent command-line access.
It uses the [ScssPhp library](https://scssphp.github.io/scssphp/).


REQUIREMENTS
------------

This module requires the ScssPhp library to be in a supported library location.
/sites/all/libraries/scssphp is preferred.

The project should be installed so that the final path looks like this:

  /sites/all/libraries/scssphp/scss.inc.php

PHP install method:
```
shell_exec('cd sites/all/libraries && mkdir scssphp && cd scssphp
&& composer require scssphp/scssphp');
```

Github:
  https://github.com/scssphp/scssphp

Github download link:
  https://codeload.github.com/scssphp/scssphp/zip/master

Website/documentation:
  https://scssphp.github.io/scssphp/


INSTALLATION
------------

Install the SCSS module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

    1. Navigate to Administration > Extend and enable the SCSS module.
    2. Navigate to Administration > Configuration > Development > Scss Compiler.
    3. To actively watch for changes, check the box at the top of the page.
    4. To select the theme to compile, select it from the drop-down list. Only
       installed themes are listed.
    5. To watch additional themes or modules, place their root paths (not
       including the scss or css directories) in the textfield, separated by a
       new line.
    6. Enter the scss and css directory names in the appropriate boxes. At this
       time, the module does not support different subfolder names,
       e.g. themes/theme/scss and modules/module/sass.
    7. Configure various output parameters.
    8. Optionally press "Test" to see the output that will be written.
    9. Press "Compile and Write" to perform a single compile if you are not
       watching for changes.
    10. A Drupal message will appear each time new changes are written to the
        filesystem.

MAINTAINERS
-----------

Jason P. Salter <jason@fivepaths.com>
and Tyler Brown Cifu Shuster <tyler@fivepaths.com>
