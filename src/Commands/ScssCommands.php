<?php

namespace Drupal\scss\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\scss\Commands
 */
class ScssCommands extends DrushCommands {

  /**
   * Force compile SCSS files.
   *
   * @command scss:compile
   * @aliases scss scss-c
   * @usage scss:compile
   */
  public function compileScss() {
    $scssCompiler = \Drupal::service('scss.compiler');
    $scssCompiler->force = TRUE;
    $scssCompiler->compileScss();
    $this->output()->writeln('SCSS compiling finished.');
  }

}
