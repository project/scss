<?php

namespace Drupal\scss\Form;

use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigInstallerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use ScssPhp\ScssPhp\OutputStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\scss\Services\ScssCompilerInterface;

/**
 * Config form. {@inheritDoc}.
 *
 * @package Drupal\scss
 */
class ScssSettingsForm extends ConfigFormBase {

  /**
   * The compiler for this settings form.
   *
   * @var \Drupal\scss\Services\ScssCompilerInterface
   */
  protected $scssCompiler;

  /**
   * The injected state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The injected theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The injected config installer.
   *
   * @var \Drupal\Core\Config\ConfigInstallerInterface
   */
  protected $configInstaller;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The injected config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The injected state.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The injected theme handler.
   * @param \Drupal\Core\Config\ConfigInstallerInterface $config_installer
   *   The injected config installer.
   * @param \Drupal\scss\Services\ScssCompilerInterface $scssCompiler
   *   The object that will do the compiling.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, ThemeHandlerInterface $theme_handler, ConfigInstallerInterface $config_installer, ScssCompilerInterface $scssCompiler) {
    $this->state = $state;
    $this->themeHandler = $theme_handler;
    $this->configInstaller = $config_installer;
    $this->scssCompiler = $scssCompiler;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
        $container->get('config.factory'),
        $container->get('state'),
        $container->get('theme_handler'),
        $container->get('config.installer'),
        $container->get('scss.compiler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scss_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'scss.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scss.settings');

    $messages = [];
    $files = [];
    foreach ($this->scssCompiler->getListToCompile() as $path_data) {

      // Get last compile.
      $scss_last = $this->state->get('scss_last_compile_date_' . $path_data['directory_path']);
      if ($scss_last) {
        $messages[] = $this->t(
          'Last compile for %dir was at @date',
          [
            '%dir' => $path_data['directory_path'],
            '@date' => date("Y-m-d H:i", $scss_last),
          ]
        );
      };

      $files = array_merge($files, $path_data['files']);

    }

    if (count($files)) {
      $messages[] = $this->t('Files to compile <i>%files</i>', ['%files' => implode(', ', $files)]);
    }

    if (count($messages)) {
      $form['last'] = [
        '#markup' => '<strong>' . implode('<br />', $messages) . '</strong>',
      ];
    }

    $form['active'] = [
      '#title' => $this
        ->t('Watch for theme changes and compile SASS/SCSS on Drupal page load'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('active'),
    ];

    $options = [];
    foreach ($this->themeHandler->listInfo() as $theme_key => $theme_data) {
      $options[$theme_key] = $theme_data->info['name'];
    }

    $form['theme_to_watch'] = [
      '#title' => $this->t('Theme'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $config->get('theme_to_watch'),
    ];

    $form['scss_directory'] = [
      '#title' => $this->t('Directory name containing SASS/SCSS source files'),
      '#type' => 'textfield',
      '#default_value' => $config->get('scss_directory'),
      '#description' => $this->t('This is usually <i>scss</i> or <i>scss</i> and is relative to theme root.'),
    ];

    $form['css_directory'] = [
      '#title' => $this->t('Directory name for CSS files destination'),
      '#type' => 'textfield',
      '#default_value' => $config->get('css_directory'),
      '#description' => $this->t('This is usually <i>css</i> and is relative to theme root.'),
    ];

    $theme = $config->get('theme_to_watch');
    $theme_directory = \Drupal::service('extension.list.theme')->getPath($theme);

    $form['additional_import_paths'] = [
      '#title' => $this->t('Additional import paths'),
      '#type' => 'textarea',
      '#default_value' => $config->get('additional_import_paths'),
      '#description' => $this
        ->t("One path per line. The theme directory path will be prepended to each path. Currently: '@theme_directory/'.", ['@theme_directory' => $theme_directory]),
    ];

    $form['additional_paths_to_watch'] = [
      '#title' => $this->t('Additional directory paths to compile and watch'),
      '#type' => 'textarea',
      '#default_value' => $config->get('additional_paths_to_watch'),
      '#description' => $this
        ->t("One path per line. Full path from web root is required. This is helpful if you are developing one or more modules that include CSS and you would like to generate the CSS files from a SASS/SCSS file."),
    ];

    $form['files_to_ignore'] = [
      '#title' => $this->t('Files to ignore'),
      '#type' => 'textarea',
      '#default_value' => $config->get('files_to_ignore'),
      '#description' => $this
        ->t('One path per line. Full path from web root. SCSS files not to compile.')
    ];

    $options = [];
    foreach (OutputStyle::cases() as $value) {
      $options[$value->value] = $value->name;
    }

    $form['output_formatting'] = [
      '#title' => $this->t('Output formatting'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $config->get('output_formatting'),
    ];

    $form['source_maps'] = [
      '#title' => $this->t('Enable source maps'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('source_maps'),
    ];

    $form['actions']['force_compile'] = [
      '#value' => $this->t('Compile and write'),
      '#type' => 'button',
      '#name' => 'compile',
      '#weight' => 1000,
    ];

    $form['actions']['test'] = [
      '#value' => $this->t('Test'),
      '#type' => 'button',
      '#name' => 'test',
      '#weight' => 1000,
    ];

    $form['actions']['reset'] = [
      '#value' => $this->t('Reset'),
      '#type' => 'submit',
      '#name' => 'reset',
      '#weight' => 1000,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (!$this->scssCompiler) {
      // If compiler is invalid, will be unset.
      return;
    }

    if ($form_state->getTriggeringElement()['#name'] == 'test') {
      $this->scssCompiler->force = TRUE;
      $this->scssCompiler->testing = TRUE;
      $this->scssCompiler->compileScss();
    }

    if ($form_state->getTriggeringElement()['#name'] == 'compile') {
      $this->scssCompiler->force = TRUE;
      $this->scssCompiler->testing = FALSE;
      $this->scssCompiler->compileScss();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Reinstall configuration options (AKA reset values).
    if ($form_state->getTriggeringElement()['#name'] == 'reset') {
      $this->configInstaller->installDefaultConfig('module', 'scss');
      return;
    }

    // Get current settings.
    $settings = $this->configFactory->getEditable('scss.settings');

    // Update settings.
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $settings->set($key, $value);
    }

    // Save settings.
    $settings->save();

    try {
      $this->scssCompiler->checkConfiguration();
    }
    catch (ConfigException $e) {
    }


  }

}
