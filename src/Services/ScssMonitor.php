<?php

namespace Drupal\scss\Services;

// This is the interface we are going to implement.
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Listens to page loads and compiles if needed.
 *
 * @category Developer_Tools
 * @package Drupal\scss
 * @link https://www.drupal.org/project/scss
 */
class ScssMonitor implements EventSubscriberInterface {

  /**
   * The injected config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The injected compiler.
   *
   * @var \Drupal\scss\Services\ScssCompilerInterface
   */
  private $compiler;

  /**
   * ScssMonitor constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The injected factory.
   * @param \Drupal\scss\Services\ScssCompilerInterface $compiler
   *   The injected compiler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ScssCompilerInterface $compiler) {
    $this->configFactory = $config_factory;
    $this->compiler = $compiler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['compileIfNeeded'];
    return $events;
  }

  /**
   * The function which runs on each page load to check if SCSS is active.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to which to respond. Not used.
   */
  public function compileIfNeeded(RequestEvent $event) {

    // Don't compile if not logged in
    if (!\Drupal::currentUser()->isAuthenticated()) {
      return;
    }

    // Check to see if we're monitoring for SASS changes.
    if (!$this->configFactory->get('scss.settings')->get('active')) {
      return;
    }

    $this->compiler->compileScss();

  }

}
