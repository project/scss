<?php

namespace Drupal\scss\Services;

/**
 * An interface describing the methods that an SCSS Compiler should implement.
 */
interface ScssCompilerInterface {

  /**
   * Determines if the save config options are valid.
   *
   * @throws \Drupal\Core\Config\ConfigException
   */
  public function checkConfiguration(): bool;

  /**
   * Assembles the list of files to compile in each directory watched.
   *
   * @return array
   *   An array of associative arrays containing path data.
   */
  public function getListToCompile(): array;

  /**
   * Compile all configured SCSS files.
   *
   * @return bool
   *   Whether or not the compilation completed.
   */
  public function compileScss(): bool;

  /**
   * Gets a list of compilable SCSS files in a directory.
   *
   * @param string $directory
   *   The directory in which to check for scss files.
   *
   * @return array
   *   The list of .scss files to compile
   */
  public function getScssFilesList(string $directory): array;

  /**
   * Checks for changes within the theme directory's SASS directory.
   *
   * @return bool
   *   Whether or not compilation is needed.
   */
  public function compileNeeded();

}
