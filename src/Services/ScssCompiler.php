<?php

namespace Drupal\scss\Services;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Exception;
use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The class that does the interface between Drupal and ScssPhp.
 *
 * @category Developer_Tools
 * @package Drupal\scss
 * @link https://www.drupal.org/project/scss
 */
class ScssCompiler implements ScssCompilerInterface {
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Whether or not to force a compilation if it doesn't need it.
   *
   * @var bool
   */
  public $force = FALSE;

  /**
   * Whether or not the compilation is just a test.
   *
   * @var bool
   */
  public $testing = FALSE;

  /**
   * The Drupal config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $settings;

  /**
   * The injected request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The injected state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * The assembled list to compile.
   *
   * @var array
   */
  private $listToCompile = [];

  /**
   * ScssCompiler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The injected factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The injected request.
   * @param \Drupal\Core\State\StateInterface $state
   *   The injected state.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request, StateInterface $state) {
    // Get settings.
    $this->settings = $config_factory->get('scss.settings');
    $this->request = $request->getCurrentRequest();
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function checkConfiguration(): bool {

    $valid = TRUE;

    // Validation - library exists.
    if (!class_exists('\ScssPhp\ScssPhp\Compiler')) {
      $library_path = 'sites/all/libraries/scssphp/scss.inc.php';
      if (!file_exists($library_path)) {
        $valid = FALSE;
        $error = $this->t(
          'Cannot locate SCSS library: @dir',
          ['@dir' => $library_path]
        );
        $this->messenger()->addError($error);
      }
      else {
        include_once $library_path;
      }
    }

    foreach ($this->getListToCompile() as $path_data) {

      // Validation - source exists.
      if (!file_exists($path_data['scss_directory_full_path'])) {
        $valid = FALSE;
        $error = $this->t(
              'Cannot locate source SCSS directory: @dir',
              ['@dir' => $path_data['directory_path']]
          );
        $this->messenger()->addError($error);
      }

      // Validating - destination writable.
      if (!is_writable($path_data['css_directory_full_path'])) {
        $valid = FALSE;
        $error = $this->t(
              'CSS directory is not writable: @dir',
              ['@dir' => $path_data['css_directory_full_path']]
          );
        $this->messenger()->addError($error);
      }

      $files = $this->getScssFilesList($path_data['scss_directory_full_path']);

      // Validating - source scss files.
      if (!count($files)) {
        $valid = FALSE;
        $error = $this->t(
              'There are no .scss files to compile for:  @dir',
              ['@dir' => $path_data['scss_directory_full_path']]
          );
        $this->messenger()->addError($error);
      }
    }

    if (!$valid) {
      throw new ConfigException($this->t('SCSS Configuration Invalid'));
    }

    return $valid;
  }

  /**
   * {@inheritdoc}
   */
  public function getListToCompile(): array {

    if (!empty($this->listToCompile)) {
      return $this->listToCompile;
    }

    $directory_list = [
      \Drupal::service('extension.list.theme')->getPath($this->settings->get('theme_to_watch')),
    ];

    $additional_paths = $this->settings->get('additional_paths_to_watch');
    foreach (explode("\n", $additional_paths) as $additional_path) {
      $directory_list[] = trim($additional_path);
    }

    $directory_list = array_filter($directory_list);

    $output = array_map(
          function ($directory_path) {
              return [
                'directory_path' => $directory_path,
                'scss_directory_full_path' => "{$directory_path}/{$this->settings->get('scss_directory')}",
                'css_directory_full_path' => "{$directory_path}/{$this->settings->get('css_directory')}",
                'files' => $this->getScssFilesList("{$directory_path}/{$this->settings->get('scss_directory')}"),
              ];
          }, $directory_list
      );
    
    $this->listToCompile = $output;

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function compileScss(): bool {

    // Validation first.
    try {
      $this->checkConfiguration();
    }
    catch (ConfigException $e) {
      return FALSE;
    }

    // If no compile is needed and we're not forcing a compilation then quit.
    if (!$this->compileNeeded() && !$this->force) {
      return FALSE;
    }

    // Loop across directories to monitor.
    foreach ($this->getListToCompile() as $path_data) {

      if (!is_writable($path_data['css_directory_full_path'])) {
        continue;
      }

      // Loop across SASS files in each directory.
      foreach ($path_data['files'] as $scss_filename) {

        $filename = pathinfo($scss_filename, PATHINFO_FILENAME);
        $scss_full_path = $path_data['scss_directory_full_path'] . "/$scss_filename";

        // Create compiler.
        $scss = new Compiler();

        // Add import path to SASS directory.
        $scss->setImportPaths($path_data['scss_directory_full_path']);

        // Add in additional import paths.
        if (!empty($this->additional_import_paths)) {
          foreach (explode("\n", $this->additional_import_paths) as $path) {
            $scss->addImportPath($this->settings->get('theme_directory') . '/' . trim($path));
          }
        }

        // Set output formatting.
        $scss->setOutputStyle(OutputStyle::from($this->settings->get('output_formatting')));

        // Enable source maps.
        if ($this->settings->get('source_maps') && !$this->testing) {

          $scss->setSourceMap(Compiler::SOURCE_MAP_FILE);
          $scss->setSourceMapOptions(
                [
                  'sourceMapWriteTo' => "{$path_data['css_directory_full_path']}/{$filename}.css.map",
                  'sourceMapURL' => "/{$path_data['css_directory_full_path']}/{$filename}.css.map",
                  'sourceMapFileName' => $scss_filename,
                  'sourceMapBasepath' => $path_data['directory_path'],
                  'sourceRoot' => '/' . $path_data['directory_path'],
                ]
            );
        }

        // Compile will throw exceptions if there are SASS errors.
        try {
          $stylesheet = file_get_contents($scss_full_path);
          \Drupal::moduleHandler()->alter('scss_preprocess', $stylesheet);
          $compiled = @$scss->compileString(
            $stylesheet,
            $scss_filename
          );
        }
        catch (Exception $e) {
          $error = $this->t(
                'Error compiling SASS file `@file`: <pre>@error</pre>',
                [
                  '@error' => $e->getMessage(),
                  '@file' => $scss_filename,
                ]
            );
          $this->messenger()->addError($error);
          continue;
        }

        // Skip writing out file if testing.
        if ($this->testing) {
          $message = new TranslatableMarkup(
                '<h2>@file : @size </h2><pre>@compiled</pre>',
                [
                  '@file' => $scss_filename,
                  '@size' => DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.2.0', fn() => ByteSizeMarkup::create(strlen($compiled->getCss())), fn() => format_size(strlen($compiled->getCss()))),
                  '@compiled' => $compiled->getCss(),
                ]
            );
          $this->messenger()->addMessage($message);
          continue;
        }

        // Write results if no exceptions thrown.
        if ($size = file_put_contents($path_data['css_directory_full_path'] . "/{$filename}.css", $compiled->getCss())) {
          file_put_contents($path_data['css_directory_full_path'] . "/{$filename}.css.map", $compiled->getSourceMap());
          // Name space the last compile times to support multiple themes.

          $this->messenger()->addMessage(
                $this
                  ->t(
                    'SCSS Compiled and saved: @file : @size',
                    ['@file' => $scss_filename, '@size' => DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.2.0', fn() => ByteSizeMarkup::create($size), fn() => format_size($size))]
                )
            );
        }
        
        $this->state->set('scss_last_compile_date_' . $path_data['directory_path'], time());
      }
    }
    return TRUE;
  }

  public function getScssFilesList(string $directory): array {

    $output = [];

    if (!file_exists($directory)) {
      return $output;
    }

    $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory));

    $files_to_ignore = array_map('trim', explode("\n", $this->settings->get('files_to_ignore') ?? ''));

    foreach ($iterator as $file) {

      if ($file->isDir()){
        continue;
      }

      $pathinfo = pathinfo((string) $file);

      if (strpos($pathinfo['filename'], '_') === 0) {
        continue;
      }

      if ($pathinfo['extension'] !== 'scss') {
        continue;
      }
      
      if (in_array($file->getPathname(), $files_to_ignore)) {
        continue;
      }

      $file_uri = str_replace($directory .'/', '', $file->getPathname());

      $output[] = $file_uri;
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function compileNeeded() {

    foreach ($this->getListToCompile() as $path_data) {
      $last_modification_date = scss_recursively_get_latest_modification_date("{$path_data['directory_path']}/{$this->settings->get('scss_directory')}");
      // Get last compile.
      $scss_last_compile_date = $this->state->get('scss_last_compile_date_' . $path_data['directory_path']);

      // Return if compile is out of date.
      if ($last_modification_date > $scss_last_compile_date) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
