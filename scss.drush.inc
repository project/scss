<?php

/**
 * @file
 * Drush integration.
 *
 * Contains functions only needed for drush integration.
 */

use Drupal\scss\Services\ScssCompiler;

/**
 * Implements hook_drush_command().
 */
function scss_drush_command() {
  $items = [];

  $items['scss'] = [
    'description' => 'Compile SASS/SCSS.',
    'callback' => 'drush_scss',
  ];

  return $items;
}

/**
 * Compile the SASS/SCSS files.
 */
function drush_scss() {

  $compiler = new ScssCompiler();
  $compiler->force = TRUE;
  $compiler->compileScss();

}
